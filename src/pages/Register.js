import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Register() {

	const {user} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whetther submit is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	console.log(email);
	console.log(password1);
	console.log(password2);

	function registerUser(e){
		e.preventDefault();

		// Clean input fields
		setEmail('');
		setPassword1('');
		setPassword2('');

		alert('Thank you for registering!');
	}

	useEffect(() => {
		// Validation to enable submit button when all fields are populatedand both password match.
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password1, password2]);

	return(
		(user.id !== null) ?
			<Navigate to="/courses" />
		:
		<Form onSubmit={(e) => registerUser(e)}>
			<h1>Register</h1>
			  <Form.Group className="mb-3" controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value={email}
			    	onChange={e => setEmail(e.target.value)}
			    	required
			    />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password1}
			    	onChange={e => setPassword1(e.target.value)}
			    	required
			    />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password2">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Verify Password" 
			    	value={password2}
			    	onChange={e => setPassword2(e.target.value)}
			    	required
			    />
			  </Form.Group>
			  {/*Conditionaly render submit button on isActive state*/}
			  { isActive ?
				  	<Button variant="primary" type="submit" id="submitBtn">
				    	Submit
				  	</Button>
				  	:
				  	<Button variant="danger" type="submit" id="submitBtn" disabled>
				    	Submit
				  	</Button>
				}

			</Form>
	)
}
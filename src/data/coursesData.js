const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: " Some quick example text to build on the card title and make up the bulk of the card's content.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Phyton - Django",
		description: " Some quick example text to build on the card title and make up the bulk of the card's content.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: " Some quick example text to build on the card title and make up the bulk of the card's content.",
		price: 55000,
		onOffer: true
	},
];
export default coursesData;